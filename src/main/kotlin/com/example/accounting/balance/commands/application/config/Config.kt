package com.example.accounting.balance.commands.application.config

import org.springframework.amqp.core.AmqpTemplate
import org.springframework.amqp.core.Queue
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory
import org.springframework.amqp.rabbit.connection.ConnectionFactory
import org.springframework.amqp.rabbit.core.RabbitAdmin
import org.springframework.amqp.rabbit.core.RabbitTemplate
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.integration.amqp.dsl.Amqp
import org.springframework.integration.amqp.outbound.AmqpOutboundEndpoint
import org.springframework.integration.annotation.ServiceActivator
import org.springframework.integration.channel.DirectChannel
import org.springframework.integration.dsl.IntegrationFlows

@Configuration
class Config {

    @Bean
    fun connectionFactory() = CachingConnectionFactory("172.25.0.2")

    @Bean
    fun rabbitAdmin(connectionFactory: ConnectionFactory) = RabbitAdmin(connectionFactory)

    @Bean
    fun rabbitTemplate(connectionFactory: ConnectionFactory) = RabbitTemplate(connectionFactory)

    @Bean
    fun statementQueue() = Queue("statementQueue")

    @Bean
    fun statementOutboundChannel() = DirectChannel()

    @Bean
    @ServiceActivator(inputChannel = "statementChannel")
    fun statementOutbound(template: AmqpTemplate): AmqpOutboundEndpoint {
        val outbound = AmqpOutboundEndpoint(template)
        outbound.setRoutingKey("statementQueue")
        return outbound
    }
}