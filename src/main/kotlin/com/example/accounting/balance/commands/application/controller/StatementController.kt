package com.example.accounting.balance.commands.application.controller

import com.example.accounting.balance.commands.application.integration.StatementGateway
import com.example.accounting.balance.commands.application.dto.StatementDto
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class StatementController(private val statementGateway: StatementGateway) {

    @GetMapping(value = ["/test"])
    fun test() {
        println("test")
        statementGateway.send(StatementDto())
    }

    @PostMapping(value = ["/statement/save"])
    fun save(dto: StatementDto) = statementGateway.send(dto)
}