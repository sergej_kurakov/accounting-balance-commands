package com.example.accounting.balance.commands.application.dto

import java.io.Serializable
import java.math.BigDecimal

data class StatementDto(var sum: BigDecimal = BigDecimal.ZERO): Serializable