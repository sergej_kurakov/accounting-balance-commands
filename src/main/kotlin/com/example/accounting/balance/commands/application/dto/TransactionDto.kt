package com.example.accounting.balance.commands.application.dto

import java.math.BigDecimal

data class TransactionDto(val transactionSum: BigDecimal)