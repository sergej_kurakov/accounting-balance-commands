package com.example.accounting.balance.commands.application.integration

import com.example.accounting.balance.commands.application.dto.StatementDto
import org.springframework.integration.annotation.MessagingGateway

@MessagingGateway(defaultRequestChannel = "statementChannel")
interface StatementGateway {
    fun send(dto: StatementDto)
}