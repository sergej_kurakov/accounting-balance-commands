package com.example.accounting.balance.commands.application.main

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.ComponentScan
import org.springframework.integration.annotation.IntegrationComponentScan

@SpringBootApplication
@ComponentScan(basePackages = ["com.example"])
@IntegrationComponentScan(basePackages = ["com.example"])
class Main

fun main(args: Array<String>) {
    runApplication<Main>(*args)
}